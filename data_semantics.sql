-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 23, 2021 at 12:54 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.2.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `data_semantics`
--

-- --------------------------------------------------------

--
-- Table structure for table `flight`
--

CREATE TABLE `flight` (
  `id` int(11) NOT NULL,
  `flight_no` int(11) NOT NULL,
  `flight_name` varchar(100) NOT NULL,
  `seat` int(11) NOT NULL,
  `type` enum('Domestic','International') NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flight`
--

INSERT INTO `flight` (`id`, `flight_no`, `flight_name`, `seat`, `type`, `status`, `created_at`, `updated_at`) VALUES
(171, 1614035136, 'IndiGo', 60, 'Domestic', '1', '2021-02-23 04:35:36', NULL),
(172, 1614035181, 'Air India', 120, 'International', '1', '2021-02-23 04:36:21', NULL),
(173, 1614035224, 'SpiceJet', 110, 'Domestic', '1', '2021-02-23 04:37:04', NULL),
(174, 1614035293, 'Vistara', 240, 'International', '1', '2021-02-23 04:38:13', NULL),
(175, 1614057680, 'Kingfisher', 240, 'International', '1', '2021-02-23 10:51:20', NULL),
(176, 1614059052, 'GoAir', 40, 'Domestic', '1', '2021-02-23 11:14:12', NULL),
(177, 1614059098, 'AirAsia India', 210, 'International', '1', '2021-02-23 11:14:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `flight_booking`
--

CREATE TABLE `flight_booking` (
  `id` int(11) NOT NULL,
  `passenger_id` int(11) NOT NULL,
  `travelling_date` date NOT NULL,
  `flight_no` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flight_booking`
--

INSERT INTO `flight_booking` (`id`, `passenger_id`, `travelling_date`, `flight_no`, `amount`, `status`, `created_at`, `updated_at`) VALUES
(8, 14, '2020-10-29', 26, 0, '1', '2021-02-23 14:39:31', '2021-02-23 15:49:13'),
(9, 14, '2020-10-29', 26, 0, '1', '2021-02-23 14:39:43', '2021-02-23 15:49:15'),
(10, 14, '2020-10-29', 26, 0, '1', '2021-02-23 14:40:03', '2021-02-23 15:49:17'),
(11, 14, '2020-10-29', 26, 0, '1', '2021-02-23 14:41:35', '2021-02-23 15:49:19'),
(12, 15, '2020-10-29', 26, 455, '1', '2021-02-23 14:42:11', '2021-02-23 15:49:21'),
(13, 16, '2020-11-30', 22, 2332, '1', '2021-02-23 17:00:52', NULL),
(14, 17, '2021-10-30', 29, 54, '1', '2021-02-23 17:16:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `flight_schedule`
--

CREATE TABLE `flight_schedule` (
  `id` int(11) NOT NULL,
  `flight_id` int(11) NOT NULL,
  `start_from` varchar(122) NOT NULL,
  `end_to` varchar(122) NOT NULL,
  `arrival` time NOT NULL,
  `departure` time NOT NULL,
  `days` varchar(250) NOT NULL,
  `amount` int(11) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1' COMMENT '0=>Inactive, 1=>active',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `flight_schedule`
--

INSERT INTO `flight_schedule` (`id`, `flight_id`, `start_from`, `end_to`, `arrival`, `departure`, `days`, `amount`, `status`, `created_at`, `updated_at`) VALUES
(22, 163, 'delhi', 'gkp34', '10:58:00', '12:59:00', 'sun,mon,wed,sat', 654, '1', '2021-02-23 00:19:22', '2021-02-23 01:51:08'),
(23, 164, 'jhgj', 'gj', '10:58:00', '11:58:00', 'sun,mon,sat', 34, '1', '2021-02-23 00:46:07', '2021-02-23 00:49:31'),
(24, 165, 'jhgj', 'jhgj', '11:57:00', '09:58:00', 'sun,mon,tue,wed,thu,fri,sat', 23, '1', '2021-02-23 00:50:25', NULL),
(25, 170, 'jhk', '54', '10:58:00', '10:59:00', 'mon,tue', 546, '1', '2021-02-23 03:25:29', NULL),
(26, 171, 'Gorakhpur', 'Delhi', '11:59:00', '23:59:00', 'sun,mon,tue', 8000, '1', '2021-02-23 04:35:37', NULL),
(27, 172, 'Delhi', 'Banglore', '22:58:00', '22:59:00', 'sun,mon,tue,wed,thu,fri,sat', 12000, '1', '2021-02-23 04:36:21', NULL),
(28, 173, 'Delhi', 'Gorakhpur', '10:58:00', '12:58:00', 'sun,mon,thu', 6000, '1', '2021-02-23 04:37:04', NULL),
(29, 174, 'Delhi', 'Los Angeles', '11:58:00', '11:59:00', 'sun,fri', 25000, '1', '2021-02-23 04:38:13', NULL),
(30, 175, 'Delhi', 'Heathrow Airport', '12:58:00', '19:59:00', 'sun,tue,wed,thu,fri', 54000, '1', '2021-02-23 10:51:20', NULL),
(31, 176, 'Delhi', 'Gorakhpur', '23:59:00', '10:59:00', 'sun,tue,wed,thu', 3600, '1', '2021-02-23 11:14:12', NULL),
(32, 177, 'Mumbai', 'Dubai', '10:59:00', '11:59:00', 'sun,mon,tue,thu,fri,sat', 12000, '1', '2021-02-23 11:14:58', NULL),
(33, 178, 'delhi', 'gorakhpur', '11:57:00', '10:59:00', 'mon,tue,wed', 1250, '1', '2021-02-23 16:59:38', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `passenger`
--

CREATE TABLE `passenger` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `mobile` int(11) NOT NULL,
  `email` varchar(111) NOT NULL,
  `city` varchar(66) NOT NULL,
  `dob` date NOT NULL,
  `gender` enum('male','female') NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `passenger`
--

INSERT INTO `passenger` (`id`, `name`, `mobile`, `email`, `city`, `dob`, `gender`, `status`, `created_at`, `updated_at`) VALUES
(15, 'mickey', 3543543, 'manj@dfd.com', 'hjfgh', '2020-09-30', 'female', '1', '2021-02-23 14:42:11', '2021-02-23 16:50:12'),
(17, 'final testing', 888888, 'final@dfdf.df', 'dfd', '2021-09-30', 'male', '1', '2021-02-23 17:16:50', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `flight`
--
ALTER TABLE `flight`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flight_booking`
--
ALTER TABLE `flight_booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `flight_schedule`
--
ALTER TABLE `flight_schedule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `passenger`
--
ALTER TABLE `passenger`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `flight`
--
ALTER TABLE `flight`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=179;

--
-- AUTO_INCREMENT for table `flight_booking`
--
ALTER TABLE `flight_booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `flight_schedule`
--
ALTER TABLE `flight_schedule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `passenger`
--
ALTER TABLE `passenger`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
