<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
include_once 'config/connection.php';
include_once 'models/flight.php';
include_once 'config/config.php';

$header =getallheaders();
$config =new Config();
if(!isset($header['token']))
{
    http_response_code(401);
    echo json_encode(['status'=>false,'message'=>'Invalid header parameter.']);
}
elseif($config->getToken()==$header['token'])
{
    $response =[];
    try{
        $database = new Connection();
        $db = $database->getConnection();
        
        $flight = new Flight($db);
        $data = json_decode(file_get_contents("php://input"));
        if(!empty($data->flight_name) && !empty($data->seat)){
            $flight->flight_name =$data->flight_name;
            $flight->seat =$data->seat;
            $flight->type =$data->type;
            $res =$flight->AddFlight();
            if($res['status']){
                $schedule_data =[];
                $flight->id =$res['last_id'];
                for($i=0; $i<count($data->start_from); $i++)
                {
                    $days =json_decode(json_encode($data->day),true);
                    $day =implode(',',array_keys($days));
                    $schedule_data[] =['flight_id'=>$flight->id,'start_from'=>$data->start_from[$i],'end_to'=>$data->end_to[$i],'arrival'=>$data->arrival[$i],'departure'=>$data->departure[$i],'days'=>$day,'amount'=>$data->amount[$i]];
                }
                $flight->schedule_data =$schedule_data;
                $res2 =$flight->AddFlightSchedule();
                http_response_code(201);
                $response['status'] =true;
                $response['message'] ="Flight added successfully.";
                echo json_encode($response);
            }
            else{
                http_response_code(503);
                $response['status'] =false;
                $response['message'] ="Unable to add flight.";
                echo json_encode($response);
            }
        }
        else{
            http_response_code(400);
            $response['status'] =false;
            $response['message'] ="Invalid input parameter.";
            echo json_encode($response);
        }
    }
    catch(Exception $e)
    {
        http_response_code(503);
        $response['status'] =false;
        $response['message'] ="Something went wrong.";
        echo json_encode($response);
    }
    
}
else
{
    http_response_code(401);
    echo json_encode(['status'=>false,'message'=>'Unauthorized access.']);
}
?>