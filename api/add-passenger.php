<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
include_once 'config/connection.php';
include_once 'models/passenger.php';
include_once 'config/config.php';

$header =getallheaders();
$config =new Config();
if(!isset($header['token']))
{
    http_response_code(401);
    echo json_encode(['status'=>false,'message'=>'Invalid header parameter.']);
}
elseif($config->getToken()==$header['token'])
{
    try{
        $database = new Connection();
        $db = $database->getConnection();
        
        $passenger = new Passenger($db);
        $data = json_decode(file_get_contents("php://input"));
        $response =[];
        if(isset($data->name) && isset($data->mobile)){
            $passenger->name =$data->name;
            $passenger->mobile =$data->mobile;
            $passenger->email =$data->email;
            $passenger->city =$data->city;
            $passenger->dob =$data->dob;
            $passenger->gender =$data->gender;
            $res =$passenger->AddPassenger();
            if($res['status']){
                $passenger->passenger_id =$res['last_id'];
                $passenger->travelling_date =$data->travelling_date;
                $passenger->flight_no =$data->flight_no;
                $passenger->amount =$data->amount;
                $passenger->AddFlightBooking();
                http_response_code(201);
                $response['status'] =true;
                $response['message'] ="Passenger added successfully.";
                echo json_encode($response);
            }
            else{
                http_response_code(503);
                $response['status'] =false;
                $response['message'] ="Unable to add passenger.";
                echo json_encode($response);
            }
        }
        else{
            http_response_code(400);
            $response['status'] =false;
            $response['message'] ="Invalid input parameter.";
            echo json_encode($response);
        }
    }
    catch(Exception $e)
    {
        http_response_code(400);
        $response['status'] =false;
        $response['message'] =$e;
        echo json_encode($response);
    }
    
}
else
{
    http_response_code(401);
    echo json_encode(['status'=>false,'message'=>'Unauthorized access.']);
}
?>