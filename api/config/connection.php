<?php
class Connection{
    private $host = "localhost";
    private $username = "root";
    private $password = "";
    private $db_name = "data_semantics";
    public $conn;
    public function getConnection(){  
        $this->connection = null;
        try{
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
            $this->conn->exec("set names utf8");
        }catch(PDOException $exception){
            echo "Connection error: " . $exception->getMessage();
        }
        return $this->conn;
    }
}
?>