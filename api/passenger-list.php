<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once 'config/connection.php';
include_once 'config/config.php';
include_once 'models/passenger.php';

$header =getallheaders();
$config =new Config();
if(!isset($header['token']))
{
    http_response_code(401);
    echo json_encode(['status'=>false,'message'=>'Invalid header parameter.']);
}
elseif($config->getToken()==$header['token'])
{
    $customerList=[];
    try{
        $database = new Connection();
        $db = $database->getConnection();
    
        $data = json_decode(file_get_contents("php://input"));
        if(isset($data->page)){
            $passenger = new Passenger($db);
            $passenger->start =($config->PerPage()*($data->page-1));
            $passenger->end =$config->PerPage();
            $query = $passenger->GetList();
            $num = $query->rowCount();
            if($num>0){
                $customerList['status'] =true;
                $customerList['message'] ="Passenger list found.";
                $customerList["data"]=[];
                while ($row = $query->fetch()){
                    $product_item=[
                        "id" => $row['id'],
                        "name" => $row['name'],
                        "mobile" => $row['mobile'],
                        "email" => $row['email'],
                        "city" => $row['city'],
                        "gender" => $row['gender'],
                        "dob" => $row['dob'],
                    ];
                    array_push($customerList["data"], $product_item);
                }
                $query2 =$passenger->GetListCount();
                $customerList["count"]=round($query2->rowCount()/$config->PerPage());
                $customerList["per_page"]=$config->PerPage();
                http_response_code(200);
                echo json_encode($customerList);
            }
            else
            {
                $customerList['status'] =false;
                $customerList['message'] ="No flight found.";
                http_response_code(404);
                echo json_encode($customerList);
            }
        }
        else
        {
            http_response_code(422);
            echo json_encode(['status'=>false,'message'=>'Invalid input parameter.']);
        }
    }
    catch(Exception $e)
    {
        $customerList['status'] =false;
        $customerList['message'] ="Something went wring.";
        http_response_code(404);
        echo json_encode($customerList);
    }
    
}
else
{
    http_response_code(401);
    echo json_encode(['status'=>false,'message'=>'Unauthorized access.']);
}

?>