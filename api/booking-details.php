<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once 'config/connection.php';
include_once 'models/passenger.php';
include_once 'config/config.php';

$header =getallheaders();
$config =new Config();
if(!isset($header['token']))
{
    http_response_code(401);
    echo json_encode(['status'=>false,'message'=>'Invalid header parameter.']);
}
elseif($config->getToken()==$header['token'])
{
    $passengerList=[];
    try{
        $database = new Connection();
        $db = $database->getConnection();
    
        $passenger = new Passenger($db);
        $data = json_decode(file_get_contents("php://input"));
        $passenger->id =$data->id;
        $query = $passenger->GetDetails();
        $num = $query->rowCount();
        if($num>0){
            $passengerList['status'] =true;
            $passengerList['message'] ="Booking details found.";
            $passengerList["data"]=[];
            while ($row = $query->fetch()){
                $passenger->flight_no =$row['flight_no'];
                $flightDetails =$passenger->GetFlightDetails();
                $product_item=[
                    "name" => $row['name'],
                    "email" => $row['email'],
                    "mobile" => $row['mobile'],
                    "city" => $row['city'],
                    "gender" => $row['gender'],
                    "dob" => $row['dob'],
                    "travelling_date" => $row['travelling_date'],
                    "flight_name" => $flightDetails['flight_name'],
                    "start_from" => $flightDetails['start_from'],
                    "end_to" => $flightDetails['end_to'],
                    "arrival" => $flightDetails['arrival'],
                    "departure" => $flightDetails['departure'],
                    "amount" => $row['amount'],
                ];
                array_push($passengerList["data"], $product_item);
            }
            http_response_code(200);
            echo json_encode($passengerList);
        }
        else
        {
            $passengerList['status'] =false;
            $passengerList['message'] ="No passenger found.";
            http_response_code(404);
            echo json_encode($passengerList);
        }
    }
    catch(Exception $e)
    {
        $passengerList['status'] =false;
        $passengerList['message'] ="Something went wrong.";
        http_response_code(404);
        echo json_encode($passengerList);
    }
    
}
else
{
    http_response_code(401);
    echo json_encode(['status'=>false,'message'=>'Unauthorized access.']);
}
?>