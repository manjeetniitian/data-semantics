<?php
class Flight{
    private $conn;
    private $table_name = "flight";
    private $schedule_table = "flight_schedule";
    public $id;
    public $flight_no;
    public $flight_name;
    public $seat;
    public $type;
    public $status;
    public $schedule_data;
    public $update_data;
    public $start;
    public $end;
    public function __construct($db){
        $this->conn = $db;
    }

    function GetList(){
        $query ="select id, flight_no, flight_name,type, seat, status, created_at from ".$this->table_name." limit ".$this->start.", ".$this->end;
        $data = $this->conn->prepare($query);
        $data->execute();
        return $data;
    }
    function GetListCount(){
        $query ="select id, flight_no, flight_name,type, seat, status, created_at from ".$this->table_name;
        $data = $this->conn->prepare($query);
        $data->execute();
        return $data;
    }
    function GetFlight(){
        $query ="select schedule.id,flight.flight_no,flight.flight_name, schedule.start_from,schedule.end_to,schedule.arrival,schedule.departure, schedule.days,schedule.amount from flight left join flight_schedule as schedule on flight.id=schedule.flight_id";
        $data = $this->conn->prepare($query);
        $data->execute();
        return $data;
    }
    function AddFlight(){
        $this->flight_no =time();
        $this->flight_name=htmlspecialchars(strip_tags($this->flight_name));
        $this->seat=htmlspecialchars(strip_tags($this->seat));
        $this->status='1';
        $query ="insert into ".$this->table_name." (flight_no,flight_name,seat,type,status) values ('$this->flight_no','$this->flight_name',$this->seat,'$this->type','$this->status')";
        $stmt = $this->conn->prepare($query);
        if($stmt->execute()){
            return ['status'=>true,'last_id'=>$this->conn->lastInsertId()];
        }
        return ['status'=>false];
    }
    function placeholders($text, $count=0, $separator=","){
        $result = array();
        if($count > 0){
            for($x=0; $x<$count; $x++){
                $result[] = $text;
            }
        }
    
        return implode($separator, $result);
    }
    function AddFlightSchedule(){
        $this->conn->beginTransaction();
        $insert_values = [];
        $datafields =array_keys($this->schedule_data[0]);
        foreach($this->schedule_data as $d){
            $question_marks[] = '('  . $this->placeholders('?', sizeof($d)) . ')';
            $insert_values = array_merge($insert_values, array_values($d));
        }
        $sql = "INSERT INTO ".$this->schedule_table." (" . implode(",", $datafields ) . ") VALUES " .implode(',', $question_marks);
        $stmt = $this->conn->prepare ($sql);
        $stmt->execute($insert_values);
        return $this->conn->commit();
    }
    function EditFlight(){
        $this->flight_name=htmlspecialchars(strip_tags($this->flight_name));
        $this->seat=htmlspecialchars(strip_tags($this->seat));
        $this->status=htmlspecialchars(strip_tags($this->status));
        $query ="update ".$this->table_name." set flight_name='$this->flight_name',seat=$this->seat,status='$this->status' where id=$this->id";
        $stmt = $this->conn->prepare($query);
        if($stmt->execute()){
            $query2="update ".$this->schedule_table." set $this->update_data where flight_id=$this->id";
            $this->conn->prepare($query2)->execute();
            return true;
        }
        return false;
    }
    function GetDetails(){
        $query ="select schedule.id, flight.flight_no, flight.flight_name,flight.type, flight.seat, flight.status, flight.created_at, schedule.start_from, schedule.end_to,schedule.arrival,schedule.departure,schedule.amount,schedule.days  from $this->table_name as flight left join $this->schedule_table as schedule on flight.id=schedule.flight_id where flight.id=$this->id";
        $data = $this->conn->prepare($query);
        $data->execute();
        return $data;
    }
    function DeleteFlight(){
        $this->id=htmlspecialchars(strip_tags($this->id));
        $query ="delete from ".$this->table_name." where id=$this->id";
        $stmt = $this->conn->prepare($query);
        if($stmt->execute()){
            return true;
        }
        return false;
    }
}
?>