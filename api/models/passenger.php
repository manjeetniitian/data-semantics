<?php
class Passenger{
    private $conn;
    private $table_name = "passenger";
    private $booking_table = "flight_booking";
    public $id;
    public $flight_no;
    public $name;
    public $mobile;
    public $email;
    public $city;
    public $dob;
    public $gender;
    public $amount;
    public function __construct($db){
        $this->conn = $db;
    }
    function AddPassenger(){
        try{
            $customer =$this->CheckCustomer();
            if($customer->rowCount()>0)
            {
                $res =$customer->fetch(PDO::FETCH_ASSOC);
                return ['status'=>true,'last_id'=>$res['id']];
            }
            else
            {
                $this->name=htmlspecialchars(strip_tags($this->name));
                $this->mobile=htmlspecialchars(strip_tags($this->mobile));
                $this->email=htmlspecialchars(strip_tags($this->email));
                $this->city=htmlspecialchars(strip_tags($this->city));
                $this->dob=htmlspecialchars(strip_tags($this->dob));
                $this->gender=htmlspecialchars(strip_tags($this->gender));
                $query ="insert into ".$this->table_name." (name,mobile,email,city,dob,gender) values ('$this->name','$this->mobile','$this->email','$this->city','$this->dob','$this->gender')";
                $stmt = $this->conn->prepare($query);
                if($stmt->execute()){
                    return ['status'=>true,'last_id'=>$this->conn->lastInsertId()];
                }
                return ['status'=>false,'message'=>$this->conn->errorInfo()];
            }
        }
        catch(Exception $e)
        {
            return ['status'=>false,'message'=>$e];
        }
        
    }
    function AddFlightBooking(){
        try{
            $this->passenger_id=htmlspecialchars(strip_tags($this->passenger_id));
            $this->travelling_date=htmlspecialchars(strip_tags($this->travelling_date));
            $this->flight_no=htmlspecialchars(strip_tags($this->flight_no));
            $this->amount=htmlspecialchars(strip_tags($this->amount));
            $query ="insert into ".$this->booking_table." (passenger_id,travelling_date,flight_no,amount) values ('$this->passenger_id','$this->travelling_date','$this->flight_no','$this->amount')";
            $stmt = $this->conn->prepare($query);
            if($stmt->execute()){
                return ['status'=>true];
            }
            return ['status'=>false,'message'=>$this->conn->errorInfo()];
        }
        catch(Exception $e)
        {
            return ['status'=>false,'message'=>$e];
        }
    }
    function CheckCustomer(){
        $query ="select id, name, mobile, email,city, dob from $this->table_name where email='".$this->email."'";
        $data = $this->conn->prepare($query);
        $data->execute();
        return $data;
    }
    function GetList(){
        $query ="select id, name, mobile,email,city,dob, gender from ".$this->table_name." limit ".$this->start.", ".$this->end;
        $data = $this->conn->prepare($query);
        $data->execute();
        return $data;
    }
    function GetListCount(){
        $query ="select id, name, mobile,email,city,dob, gender from ".$this->table_name;
        $data = $this->conn->prepare($query);
        $data->execute();
        return $data;
    }
    function EditPassenger(){
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->mobile=htmlspecialchars(strip_tags($this->mobile));
        $this->email=htmlspecialchars(strip_tags($this->email));
        $this->city=htmlspecialchars(strip_tags($this->city));
        $this->gender=htmlspecialchars(strip_tags($this->gender));
        $this->dob=htmlspecialchars(strip_tags($this->dob));
        $query ="update ".$this->table_name." set name='$this->name',email='$this->email',mobile='$this->mobile', city='$this->city', gender='$this->gender', dob='$this->dob' where id=$this->id";
        $stmt = $this->conn->prepare($query);
        if($stmt->execute()){
            return true;
        }
        return false;
    }
    function GetDetails(){
        $query ="select booking.flight_no,passenger.name,passenger.email,passenger.mobile,passenger.gender,passenger.dob,passenger.city, booking.travelling_date,booking.amount  from $this->table_name as passenger left join $this->booking_table as booking on passenger.id=booking.passenger_id where passenger.id=$this->id";
        $data = $this->conn->prepare($query);
        $data->execute();
        return $data;
    }
    function GetFlightDetails(){
        try
        {
            $query ="select flight.flight_name, schedule.start_from,schedule.end_to,schedule.arrival,schedule.departure from flight left join flight_schedule as schedule on flight.id=schedule.flight_id where schedule.id=$this->flight_no";
            $data = $this->conn->prepare($query);
            $data->execute();
            $res =$data->fetch(PDO::FETCH_ASSOC);
            return $res;
        }
        catch(Exception $e)
        {
            return false;
        }
        
    }
    function DeletePassenger(){
        $this->id=htmlspecialchars(strip_tags($this->id));
        $query ="delete from ".$this->table_name." where id=$this->id";
        $stmt = $this->conn->prepare($query);
        if($stmt->execute()){
            return true;
        }
        return false;
    }
}
?>