<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
  
include_once 'config/connection.php';
include_once 'models/flight.php';
include_once 'config/config.php';

$header =getallheaders();
$config =new Config();
if(!isset($header['token']))
{
    http_response_code(401);
    echo json_encode(['status'=>false,'message'=>'Invalid header parameter.']);
}
elseif($config->getToken()==$header['token'])
{
    $response =[];
    try{
        $database = new Connection();
        $db = $database->getConnection();
        
        $flight = new Flight($db);
        $data = json_decode(file_get_contents("php://input"));
        if(isset($data->id) && isset($data->flight_name) && isset($data->seat)){
            $flight->id = $data->id;
            $flight->flight_name = $data->flight_name;
            $flight->seat = $data->seat;
            $flight->status = '1';
            $days =json_decode(json_encode($data->day),true);
            $day =implode(',',array_keys($days));
            $flight->update_data ="start_from='".$data->start_from[0]."', end_to='".$data->end_to[0]."', arrival='".$data->arrival[0]."', departure='".$data->departure[0]."', days='".$day."', amount='".$data->amount[0]."'";
            if($flight->EditFlight()){
                http_response_code(201);
                $response['status'] =true;
                $response['message'] ="Flight updated successfully.";
                echo json_encode($response);
            }
            else{
                http_response_code(503);
                $response['status'] =false;
                $response['message'] ="Unable to update flight.";
                echo json_encode($response);
            }
        }
        else{
            http_response_code(400);
            $response['status'] =false;
            $response['message'] ="Invalid input parameter.";
            echo json_encode($response);
        }
    }
    catch(Exception $e)
    {
        http_response_code(400);
        $response['status'] =false;
        $response['message'] ="Something went wrong.";
        echo json_encode($response);
    }
    
}
else
{
    http_response_code(401);
    echo json_encode(['status'=>false,'message'=>'Unauthorized access.']);
}
?>