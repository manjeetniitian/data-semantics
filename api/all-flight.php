<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once 'config/connection.php';
include_once 'config/config.php';
include_once 'models/flight.php';

$header =getallheaders();
$config =new Config();
if(!isset($header['token']))
{
    http_response_code(401);
    echo json_encode(['status'=>false,'message'=>'Invalid header parameter.']);
}
elseif($config->getToken()==$header['token'])
{
    $flightList=[];
    try{
        $database = new Connection();
        $db = $database->getConnection();
    
        $data = json_decode(file_get_contents("php://input"));
            $flight = new Flight($db);
            $query = $flight->GetFlight();
            $num = $query->rowCount();
            if($num>0){
                $flightList['status'] =true;
                $flightList['message'] ="Flight list found.";
                $flightList["data"]=[];
                while ($row = $query->fetch()){
                    $product_item=[
                        "id" => $row['id'],
                        "flight_no" => $row['flight_no'],
                        "flight_name" => $row['flight_name'],
                        "start_from" => $row['start_from'],
                        "end_to" => $row['end_to'],
                        "arrival" => $row['arrival'],
                        "departure" => $row['departure'],
                        "amount" => $row['amount'],
                        "days" => $row['days']
                    ];
                    array_push($flightList["data"], $product_item);
                }
                $query2 =$flight->GetListCount();
                $flightList["count"]=round($query2->rowCount()/$config->PerPage());
                $flightList["per_page"]=$config->PerPage();
                http_response_code(200);
                echo json_encode($flightList);
            }
            else
            {
                $flightList['status'] =false;
                $flightList['message'] ="No flight found.";
                http_response_code(404);
                echo json_encode($flightList);
            }
    }
    catch(Exception $e)
    {
        $flightList['status'] =false;
        $flightList['message'] ="Something went wrong.";
        http_response_code(404);
        echo json_encode($flightList);
    }
    
}
else
{
    http_response_code(401);
    echo json_encode(['status'=>false,'message'=>'Unauthorized access.']);
}

?>