<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Flight Management - Data Semantics</title>
<link  href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"  rel="stylesheet"/>
<link  href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"  rel="stylesheet"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<style>
.icon {
  width: 28px;
  height: 28px;
  color:white;
  background-color:#2181E5;
  border-radius: 50%;
  position: absolute;
  right: -7px;
  top: -7px;
  /* display:flex; */
}
</style>
</head>
<body>
<div class="container">
<header>
<?php include_once 'include/header.php' ?>
  <div class="p-5 bg-light">
  <h4 class="mb-3">Add Flight</h4>
  <?php
    if(isset($_POST['addFlight']))
    {
        include_once 'api_request.php';
        $api = new ApiRequest();
        $api->api = "add-flight.php";
        $api->method = "POST";
        $api->data =$_POST;
        if(!isset($_POST['day'])) {  echo"<div class='alert alert-danger'>Please select days.</div>";  }
        else {
            $response =$api->GetResponse();
            $response =json_decode($response,true);
            $alert =($response['status'])?'alert-success':'alert-danger';
            echo"<div class='alert $alert'>".$response['message']."</div>";
        }
    }
    ?>
    <form method="post" class="needs-validation" novalidate>
        <div class="form-row">
            <div class="col-md-6 mb-6">
                <label for="validationCustom01">Flight Name</label>
                <input type="text" class="form-control" id="validationCustom01" placeholder="Flight Name" name="flight_name" required>
                <div class="invalid-feedback">Please enter flight name.</div>
            </div>
            <div class="col-md-4 mb-3">
                <label for="validationCustom02">Flight Type</label>
                    <select class="custom-select" id="validationCustom02" name="type" required>
                    <option value="Domestic">Domestic</option>
                    <option value="International">International</option>
                    </select>
                <div class="invalid-feedback">Please select flight type.</div>
            </div>            
        </div>
        <div class="form-row">
            <div class="col-md-6 mb-3">
                <label for="validationCustom03">Seat</label>
                <input type="number" class="form-control" id="validationCustom03" name="seat" placeholder="Seat" required>
                <div class="invalid-feedback">Please enter number of available seat.</div>
            </div>
        </div>
        <hr/>
        <!-- schedule -->
            <div class="alert alert-info">
                <div class="form-row">
                    <div class="col-md-6 mb-6">
                        <label for="validationCustom01">From</label>
                        <input type="text" class="form-control" id="validationCustom01" name="start_from[]" placeholder="Start From" required>
                        <div class="invalid-feedback">Please enter flight start from.</div>
                    </div>
                    <div class="col-md-6 mb-6">
                        <label for="validationCustom01">To</label>
                            <input type="text" class="form-control" id="validationCustom01" name="end_to[]" placeholder="End To" required>
                            <div class="invalid-feedback">Please enter flight end to.</div>
                    </div>            
                </div>
                <div class="form-row">
                    <div class="col-md-6 mb-6">
                        <label for="validationCustom01">Arrival Time</label>
                        <input type="time" class="form-control" id="validationCustom01" name="arrival[]" required>
                        <div class="invalid-feedback">Please enter flight arrival time.</div>
                    </div>
                    <div class="col-md-6 mb-6">
                        <label for="validationCustom01">Departure Time</label>
                            <input type="time" class="form-control" id="validationCustom01" name="departure[]" required>
                            <div class="invalid-feedback">Please enter flight departure time.</div>
                    </div>            
                </div>
                <div class="form-row">
                    <div class="col-md-6 mb-6">
                        <label for="validationCustom01">Day</label>
                        <div class="clearfix"></div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" id="customCheck1" class="custom-control-input" name="day[sun]">
                            <label class="custom-control-label" for="customCheck1">S</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" id="customCheck2" class="custom-control-input" name="day[mon]">
                            <label class="custom-control-label" for="customCheck2">M</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" id="customCheck3" class="custom-control-input" name="day[tue]">
                            <label class="custom-control-label" for="customCheck3">T</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" id="customCheck4" class="custom-control-input" name="day[wed]">
                            <label class="custom-control-label" for="customCheck4">W</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" id="customCheck5" class="custom-control-input" name="day[thu]">
                            <label class="custom-control-label" for="customCheck5">T</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" id="customCheck6"class="custom-control-input" name="day[fri]">
                            <label class="custom-control-label" for="customCheck6">F</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" id="customCheck7"class="custom-control-input" name="day[sat]">
                            <label class="custom-control-label" for="customCheck7">S</label>
                        </div>
                    </div>
                    <div class="col-md-4 mb-4">
                        <label for="validationCustom01">Amount</label>
                            <input type="number" class="form-control" id="validationCustom01" name="amount[]" placeholder="Amount" required>
                            <div class="invalid-feedback">Please enter amount.</div>
                    </div>            
                </div>
            </div>
        <button class="btn btn-primary" name="addFlight" type="submit">Submit</button>
        </form>
  </div>
  <!-- Jumbotron -->
</header>
<!-- Button trigger modal -->

<!-- Modal -->
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
        (function() {
        'use strict';
        window.addEventListener('load', function() {
            var forms = document.getElementsByClassName('needs-validation');
            var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
            });
        }, false);
        })();
        </script>
</body>
</html>