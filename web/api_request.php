<?php
class ApiRequest
{
    public $host ="http://localhost/data_sem/api/";
    public $api;
    public $method;
    public $data;
    function GetResponse()
    {
      try
      {
        $this->data =json_encode($this->data);
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => $this->host.$this->api,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => $this->method,
          CURLOPT_POSTFIELDS => $this->data,
          CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/json",
            "token: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJQcm9qZWN0IjoiRGF0YSBTZW1hbnRpY3MiLCJEZXZlbG9wZXIiOiJNYW5qZWV0In0.Cmv6gvno0CnipNon0aeHXdE1HIzPuaMxsW-gdjKvMis"
          ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
          return $err;
        } else {
          return $response;
        }
      }
      catch(Exception $e)
      {
        return ['statys'=>'false','message'=>'Something went wrong.'];
      }
    }
}
?>