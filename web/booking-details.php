<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Flight Management - Data Semantics</title>
<link  href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"  rel="stylesheet"/>
<link  href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"  rel="stylesheet"/>
<link  href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.2.0/mdb.min.css" rel="stylesheet"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<div class="container">
<header>
<?php include_once 'include/header.php';

  include_once 'api_request.php';
  $api = new ApiRequest();
    $api->api ="booking-details.php";
    $api->method = "POST";
    $api->data =['id'=>base64_decode($_GET['p_id'])];
    $response =$api->GetResponse();
    $response =json_decode($response,true);
    if($response['status']==false) { echo "Invalid request."; return false;  }

    ?>
<div class="p-5 bg-light">
  <h4 class="mb-3">Passenger Details</h4>
  <div class="form-row">
            <div class="col-md-6 mb-6">
                <label for="validationCustom01">Passenger Name</label>
                <input type="text" class="form-control" id="validationCustom01" value="<?php echo $response['data'][0]['name']; ?>"  readonly required name="name">
                <div class="invalid-feedback">Please enter passenger name.</div>
            </div>
            <div class="col-md-6 mb-6">
                <label for="validationCustom01">Mobile</label>
                <input type="number" class="form-control" id="validationCustom01" name="mobile" value="<?php echo $response['data'][0]['mobile']; ?>" readonly required>
                <div class="invalid-feedback">Please enter passenger mobile.</div>
            </div>          
        </div>
        <div class="form-row">
            <div class="col-md-6 mb-6">
                <label for="validationCustom01">Email</label>
                <input type="email" class="form-control" id="validationCustom01" name="email" value="<?php echo $response['data'][0]['email']; ?>" readonly required>
                <div class="invalid-feedback">Please enter passenger email.</div>
            </div>
            <div class="col-md-6 mb-6">
                <label for="validationCustom01">City</label>
                <input type="text" class="form-control" id="validationCustom01" name="city" value="<?php echo $response['data'][0]['city']; ?>" readonly required>
                <div class="invalid-feedback">Please enter passenger city.</div>
            </div>          
        </div>
        <div class="form-row">
            <div class="col-md-6 mb-6">
                <label for="validationCustom01">DOB</label>
                <input type="date" class="form-control" id="validationCustom01" name="dob" value="<?php echo $response['data'][0]['dob']; ?>" readonly required>
                <div class="invalid-feedback">Please enter passenger dob.</div>
            </div>
            <div class="col-md-4 mb-3">
                <label for="validationCustom02">Gender</label>
                <div class="clearfix"></div>
                <div class="form-check form-check-inline" style="padding-left:28px;">
                    <input class="form-check-input" type="radio" name="gender" id="inlineRadio1" checked disabled value="Male" required>
                    <label class="form-check-label" for="inlineRadio1"><?php echo $response['data'][0]['gender']; ?></label>
                </div>
            </div>            
        </div>
        <hr/>
        <!-- schedule -->
        <div class="schedule">
            <?php
            foreach($response['data'] as $data)
            {
            ?>
            <div class="alert alert-info">
            <div class="form-row">
                    <div class="col-md-6 mb-6">
                        <label for="validationCustom01">Travelling Date</label>
                        <input type="date" class="form-control" id="validationCustom01" value="<?php echo $data['travelling_date']; ?>" readonly name="travelling_date" required>
                        <div class="invalid-feedback">Please enter travelling date.</div>
                    </div>
                    <div class="col-md-6 mb-6">
                        <label for="validationCustom01">Flight Name/Flight No</label>
                            <input type="text" class="form-control" id="validationCustom01" value="<?php echo $data['flight_name']; ?>" readonly name="flight_no" required>
                            <div class="invalid-feedback">Please enter flight end to.</div>
                    </div>            
                </div>
                <div class="form-row">
                    <div class="col-md-6 mb-6">
                        <label for="validationCustom01">From</label>
                        <input type="text" class="form-control" id="validationCustom01" value="<?php echo $data['start_from']; ?>" readonly name="start_from">
                        <div class="invalid-feedback">Please enter flight start from.</div>
                    </div>
                    <div class="col-md-6 mb-6">
                        <label for="validationCustom01">To</label>
                            <input type="text" class="form-control" id="validationCustom01" value="<?php echo $data['end_to']; ?>" readonly name="end_to">
                            <div class="invalid-feedback">Please enter flight end to.</div>
                    </div>            
                </div>
                <div class="form-row">
                    <div class="col-md-6 mb-6">
                        <label for="validationCustom01">Arrival Time</label>
                        <input type="time" class="form-control" id="validationCustom01" value="<?php echo $data['arrival']; ?>" readonly name="arrival">
                        <div class="invalid-feedback">Please enter flight arrival time.</div>
                    </div>
                    <div class="col-md-6 mb-6">
                        <label for="validationCustom01">Departure Time</label>
                            <input type="time" class="form-control" id="validationCustom01" value="<?php echo $data['departure']; ?>" readonly name="departure">
                            <div class="invalid-feedback">Please enter flight departure time.</div>
                    </div>            
                </div>
                <div class="form-row">
                    <div class="col-md-4 mb-4">
                        <label for="validationCustom01">Amount</label>
                            <input type="number" class="form-control" readonly id="validationCustom01" value="<?php echo $data['amount']; ?>" readonly name="amount[]" placeholder="Amount" required>
                            <div class="invalid-feedback">Please enter amount.</div>
                    </div>            
                </div>
            </div>
        </div>
        <?php } ?>
  </div>
</header>
</div>
<script  type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.2.0/mdb.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</body>
</html>