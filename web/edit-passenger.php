<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Flight Management - Data Semantics</title>
<link  href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"  rel="stylesheet"/>
<link  href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"  rel="stylesheet"/>
<link  href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.2.0/mdb.min.css" rel="stylesheet"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<div class="container">
<header>
<?php include_once 'include/header.php';
  include_once 'api_request.php';
  $api = new ApiRequest();
    if(isset($_POST['updatePassenger']))
    {
        $api->api = "edit-passenger.php";
        $api->method = "POST";
        $_POST['id'] =base64_decode($_POST['token']);
        $api->data =$_POST;
        $response =$api->GetResponse();
        $response =json_decode($response,true);
        $alert =($response['status'])?'alert-success':'alert-danger';
        echo"<div class='alert $alert'>".$response['message']."</div>";
    }
    $api->api ="booking-details.php";
    $api->method = "POST";
    $api->data =['id'=>base64_decode($_GET['p_id'])];
    $response =$api->GetResponse();
    $response =json_decode($response,true);
    if($response['status']==false) { echo "Invalid request."; return false;  }

    ?>
<div class="p-5 bg-light">
  <h4 class="mb-3">Update Passenger Details</h4>
  <form method="post" class="needs-validation" novalidate>
  <input type="hidden" value="<?php echo $_GET['p_id']; ?>" name="token">
  <div class="form-row">
            <div class="col-md-6 mb-6">
                <label for="validationCustom01">Passenger Name</label>
                <input type="text" class="form-control" id="validationCustom01" value="<?php echo $response['data'][0]['name']; ?>" required name="name">
                <div class="invalid-feedback">Please enter passenger name.</div>
            </div>
            <div class="col-md-6 mb-6">
                <label for="validationCustom01">Mobile</label>
                <input type="number" class="form-control" id="validationCustom01" name="mobile" value="<?php echo $response['data'][0]['mobile']; ?>" required>
                <div class="invalid-feedback">Please enter passenger mobile.</div>
            </div>          
        </div>
        <div class="form-row">
            <div class="col-md-6 mb-6">
                <label for="validationCustom01">Email</label>
                <input type="email" class="form-control" id="validationCustom01" name="email" value="<?php echo $response['data'][0]['email']; ?>" required>
                <div class="invalid-feedback">Please enter passenger email.</div>
            </div>
            <div class="col-md-6 mb-6">
                <label for="validationCustom01">City</label>
                <input type="text" class="form-control" id="validationCustom01" name="city" value="<?php echo $response['data'][0]['city']; ?>" required>
                <div class="invalid-feedback">Please enter passenger city.</div>
            </div>          
        </div>
        <div class="form-row">
            <div class="col-md-6 mb-6">
                <label for="validationCustom01">DOB</label>
                <input type="date" class="form-control" id="validationCustom01" name="dob" value="<?php echo $response['data'][0]['dob']; ?>" required>
                <div class="invalid-feedback">Please enter passenger dob.</div>
            </div>
            <div class="col-md-4 mb-3">
                <label for="validationCustom02">Gender</label>
                <div class="clearfix"></div>
                <div class="form-check form-check-inline" style="padding-left:28px;">
                    <input class="form-check-input" type="radio" name="gender" id="inlineRadio1" <?php echo ($response['data'][0]['gender']=='male') ?'checked':''; ?> value="Male" required>
                    <label class="form-check-label" for="inlineRadio1">Male</label>
                </div>
                <div class="form-check form-check-inline" style="padding-left:28px;">
                    <input class="form-check-input" type="radio" name="gender" id="inlineRadio2" value="Female" <?php echo ($response['data'][0]['gender']=='female') ?'checked':''; ?> required>
                    <label class="form-check-label" for="inlineRadio2">Female</label>
                </div>
                <div class="invalid-feedback">Please select gender.</div>
            </div>            
        </div>
        <hr/>
        <!-- schedule -->
        <button class="btn btn-primary" name="updatePassenger" type="submit">Update</button>
        </form>
  </div>
</header>
</div>
<script  type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.2.0/mdb.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
        (function() {
        'use strict';
        window.addEventListener('load', function() {
            var forms = document.getElementsByClassName('needs-validation');
            var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
            });
        }, false);
        })();
        </script>
</body>
</body>
</html>