<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Flight Management - Data Semantics</title>
<link  href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"  rel="stylesheet"/>
<link  href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"  rel="stylesheet"/>
<link  href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.2.0/mdb.min.css" rel="stylesheet"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<div class="container">
<header>
<?php include_once 'include/header.php';

  include_once 'api_request.php';
  $api = new ApiRequest();
    $api->api ="flight-details.php";
    $api->method = "POST";
    $api->data =['id'=>base64_decode($_GET['flight_id'])];
    $response =$api->GetResponse();
    $response =json_decode($response,true);
    if($response['status']==false) { echo "Invalid request."; return false;  }

    ?>
    <div class="p-5 bg-light">
  <h4 class="mb-3">Flight Details</h4>
        <div class="form-row">
            <div class="col-md-6 mb-6">
                <label for="validationCustom01">Flight Name</label>
                <input type="text" readonly class="form-control" id="validationCustom01" placeholder="Flight Name" value="<?php echo $response['data'][0]['flight_name']; ?>" name="flight_name" required>
                <div class="invalid-feedback">Please enter flight name.</div>
            </div>
            <div class="col-md-4 mb-3">
                <label for="validationCustom02">Flight Type</label>
                    <select class="custom-select" readonly disabled id="validationCustom02" value="<?php echo $response['data'][0]['type']; ?>" name="type" required>
                    <option value="Domestic">Domestic</option>
                    <option value="International">International</option>
                    </select>
                <div class="invalid-feedback">Please select flight type.</div>
            </div>            
        </div>
        <div class="form-row">
            <div class="col-md-6 mb-3">
                <label for="validationCustom03">Seat</label>
                <input type="number" class="form-control" readonly id="validationCustom03" name="seat" value="<?php echo $response['data'][0]['seat']; ?>" placeholder="Seat" required>
                <div class="invalid-feedback">Please enter number of available seat.</div>
            </div>
        </div>
        <hr/>
        <!-- schedule -->
        <div class="schedule">
            <?php
            foreach($response['data'] as $data)
            {
            ?>
            <div class="alert alert-info">
                <div class="form-row">
                    <div class="col-md-6 mb-6">
                        <label for="validationCustom01">From</label>
                        <input type="text" class="form-control" readonly id="validationCustom01" value="<?php echo $data['start_from']; ?>" name="start_from[]" placeholder="Start From" required>
                        <div class="invalid-feedback">Please enter flight start from.</div>
                    </div>
                    <div class="col-md-6 mb-6">
                        <label for="validationCustom01">To</label>
                            <input type="text" class="form-control" readonly id="validationCustom01" value="<?php echo $data['end_to']; ?>" name="end_to[]" placeholder="End To" required>
                            <div class="invalid-feedback">Please enter flight end to.</div>
                    </div>            
                </div>
                <div class="form-row">
                    <div class="col-md-6 mb-6">
                        <label for="validationCustom01">Arrival Time</label>
                        <input type="time" class="form-control" readonly id="validationCustom01" value="<?php echo $data['arrival']; ?>" name="arrival[]" required>
                        <div class="invalid-feedback">Please enter flight arrival time.</div>
                    </div>
                    <div class="col-md-6 mb-6">
                        <label for="validationCustom01">Departure Time</label>
                            <input type="time" class="form-control" readonly id="validationCustom01" name="departure[]" value="<?php echo $data['departure']; ?>" required>
                            <div class="invalid-feedback">Please enter flight departure time.</div>
                    </div>            
                </div>
                <?php
                $days =explode(',',$response['data'][0]['days']);
                ?>
                <div class="form-row">
                    <div class="col-md-6 mb-6">
                        <label for="validationCustom01">Day</label>
                        <div class="clearfix"></div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" id="customCheck1" class="custom-control-input" <?php echo (in_array('sun',$days)) ? 'checked' :''; ?> disabled>
                            <label class="custom-control-label" for="customCheck1">S</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" id="customCheck2"class="custom-control-input" <?php echo (in_array('mon',$days)) ? 'checked' :''; ?> disabled>
                            <label class="custom-control-label" for="customCheck2">M</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" id="customCheck3"class="custom-control-input" <?php echo (in_array('tue',$days)) ? 'checked' :''; ?> disabled>
                            <label class="custom-control-label" for="customCheck3">T</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" id="customCheck4"class="custom-control-input" <?php echo (in_array('wed',$days)) ? 'checked' :''; ?> disabled>
                            <label class="custom-control-label" for="customCheck4">W</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" id="customCheck5"class="custom-control-input" <?php echo (in_array('thu',$days)) ? 'checked' :''; ?> disabled>
                            <label class="custom-control-label" for="customCheck5">T</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" id="customCheck6"class="custom-control-input" <?php echo (in_array('fri',$days)) ? 'checked' :''; ?> disabled>
                            <label class="custom-control-label" for="customCheck6">F</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" id="customCheck7"class="custom-control-input" <?php echo (in_array('sat',$days)) ? 'checked' :''; ?> disabled>
                            <label class="custom-control-label" for="customCheck7">S</label>
                        </div>
                    </div>
                    <div class="col-md-4 mb-4">
                        <label for="validationCustom01">Amount</label>
                            <input type="number" class="form-control" readonly id="validationCustom01" value="<?php echo $data['amount']; ?>" name="amount[]" placeholder="Amount" required>
                            <div class="invalid-feedback">Please enter amount.</div>
                    </div>            
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="scheduleMore"></div>
  </div>
  <!-- Jumbotron -->
</header>
<!-- Button trigger modal -->

<!-- Modal -->
</div>
<script  type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.2.0/mdb.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</body>
</html>