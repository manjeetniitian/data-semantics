<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Flight Management - Data Semantics</title>
<link  href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"  rel="stylesheet"/>
<link  href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"  rel="stylesheet"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<style>
.icon {
  width: 28px;
  height: 28px;
  color:white;
  background-color:#2181E5;
  border-radius: 50%;
  position: absolute;
  right: -7px;
  top: -7px;
  /* display:flex; */
}
</style>
</head>
<body>
<div class="container">
<header>
  <?php include_once 'include/header.php' ?>
  <div class="p-5 bg-light">
  <h4 class="mb-3">Add Passenger</h4>
  <?php
  include_once 'api_request.php';
  $api = new ApiRequest();
    if(isset($_POST['addPassenger']))
    {
        try{
            $api->api = "add-passenger.php";
            $api->method = "POST";
            $api->data =$_POST;
            $response =$api->GetResponse();
            $response =json_decode($response,true);
            $alert =($response['status'])?'alert-success':'alert-danger';
            echo"<div class='alert $alert'>".$response['message']."</div>";
        }
        catch(Exception $e)
        {
            echo"<div class='alert alert-danger'>Something went wring.</div>";
        }
    }
    $api->api = "all-flight.php";
    $api->method = "GET";
    $api->data =[];
    $response =$api->GetResponse();
    $response =json_decode($response,true);
    ?>
    <form method="post" class="needs-validation passenger-from" novalidate>
        <div class="form-row">
            <div class="col-md-6 mb-6">
                <label for="validationCustom01">Passenger Name</label>
                <input type="text" class="form-control" id="validationCustom01" name="name" required>
                <div class="invalid-feedback">Please enter passenger name.</div>
            </div>
            <div class="col-md-6 mb-6">
                <label for="validationCustom01">Mobile</label>
                <input type="number" class="form-control" id="validationCustom01" name="mobile" required>
                <div class="invalid-feedback">Please enter passenger mobile.</div>
            </div>          
        </div>
        <div class="form-row">
            <div class="col-md-6 mb-6">
                <label for="validationCustom01">Email</label>
                <input type="email" class="form-control" id="validationCustom01" name="email" required>
                <div class="invalid-feedback">Please enter passenger email.</div>
            </div>
            <div class="col-md-6 mb-6">
                <label for="validationCustom01">City</label>
                <input type="text" class="form-control" id="validationCustom01" name="city" required>
                <div class="invalid-feedback">Please enter passenger city.</div>
            </div>          
        </div>
        <div class="form-row">
            <div class="col-md-6 mb-6">
                <label for="validationCustom01">DOB</label>
                <input type="date" class="form-control" id="validationCustom01" name="dob" required>
                <div class="invalid-feedback">Please enter passenger dob.</div>
            </div>
            <div class="col-md-4 mb-3">
                <label for="validationCustom02">Gender</label>
                <div class="clearfix"></div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="gender" id="inlineRadio1" value="Male" required>
                    <label class="form-check-label" for="inlineRadio1">Male</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input" type="radio" name="gender" id="inlineRadio2" value="Female" required>
                    <label class="form-check-label" for="inlineRadio2">Female</label>
                </div>
                <div class="invalid-feedback">Please select gender.</div>
            </div>            
        </div>
        <hr/>
        <!-- schedule -->
            <div class="alert alert-info">
                <div class="form-row">
                    <div class="col-md-6 mb-6">
                        <label for="validationCustom01">Travelling Date</label>
                        <input type="date" class="form-control" id="validationCustom01" name="travelling_date" required>
                        <div class="invalid-feedback">Please enter travelling date.</div>
                    </div>
                    <div class="col-md-6 mb-6">
                        <label for="validationCustom01">Flight Name/Flight No</label>
                        <select class="custom-select" id="validationCustom01" name="flight_no" required>
                            <option value="">select...</option>
                        <?php
                        foreach($response['data'] as $data)
                        { 
                            ?>
                            <option value="<?php echo $data['id']; ?>" onchange="getSchedule('<?php echo $data['start_from'] ?>')"><?php echo $data['flight_name']; ?></option>
                        
                            <?php
                         }
                        ?>
                        </select>
                            <div class="invalid-feedback">Please enter flight no.</div>
                    </div>            
                </div>
                <div class="form-row">
                    <div class="col-md-6 mb-6">
                        <label for="validationCustom01">From</label>
                        <input type="text" class="form-control from_to" id="validationCustom01" name="start_from" readonly>
                        <div class="invalid-feedback">Please enter flight start from.</div>
                    </div>
                    <div class="col-md-6 mb-6">
                        <label for="validationCustom01">To</label>
                            <input type="text" class="form-control" id="validationCustom01" name="end_to" readonly>
                            <div class="invalid-feedback">Please enter flight end to.</div>
                    </div>            
                </div>
                <div class="form-row">
                    <div class="col-md-6 mb-6">
                        <label for="validationCustom01">Arrival Time</label>
                        <input type="time" class="form-control" id="validationCustom01" name="arrival" readonly>
                        <div class="invalid-feedback">Please enter flight arrival time.</div>
                    </div>
                    <div class="col-md-6 mb-6">
                        <label for="validationCustom01">Departure Time</label>
                            <input type="time" class="form-control" id="validationCustom01" name="departure" readonly>
                            <div class="invalid-feedback">Please enter flight departure time.</div>
                    </div>            
                </div>
                <div class="form-row">
                    <div class="col-md-6 mb-6">
                        <label for="validationCustom01">Day</label>
                        <div class="clearfix"></div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" id="customCheck1" class="custom-control-input">
                            <label class="custom-control-label" for="customCheck1">S</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" id="customCheck2" class="custom-control-input">
                            <label class="custom-control-label" for="customCheck2">M</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" id="customCheck3" class="custom-control-input">
                            <label class="custom-control-label" for="customCheck3">T</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" id="customCheck4" class="custom-control-input">
                            <label class="custom-control-label" for="customCheck4">W</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" id="customCheck5" class="custom-control-input">
                            <label class="custom-control-label" for="customCheck5">T</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" id="customCheck6"class="custom-control-input">
                            <label class="custom-control-label" for="customCheck6">F</label>
                        </div>
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" id="customCheck7"class="custom-control-input">
                            <label class="custom-control-label" for="customCheck7">S</label>
                        </div>
                    </div>
                    <div class="col-md-4 mb-4">
                        <label for="validationCustom01">Amount</label>
                            <input type="number" class="form-control" id="validationCustom01" name="amount" placeholder="Amount">
                            <div class="invalid-feedback">Please enter amount.</div>
                    </div>            
                </div>
            </div>
        <button class="btn btn-primary" name="addPassenger" type="submit">Submit</button>
        </form>
  </div>
</header>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
        (function() {
        'use strict';
        window.addEventListener('load', function() {
            var forms = document.getElementsByClassName('needs-validation');
            var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
            });
        }, false);
        })();
        </script>
</body>
</html>