<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Flight Management - Data Semantics</title>
<link  href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"  rel="stylesheet"/>
<link  href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"  rel="stylesheet"/>
<link  href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.2.0/mdb.min.css" rel="stylesheet"/>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

</head>
<body>
<div class="container">
<header>
<?php include_once 'include/header.php' ?>
  <div class="p-5 bg-light">
  <h4 class="mb-3">Flight List</h4>
  <?php
  header('Content-Type: text/html; charset=UTF-8');
  include_once 'api_request.php';
  $api = new ApiRequest();
  if(isset($_POST['delete']))
  {
    $api->api ="delete-flight.php";
    $api->method = "POST";
    $api->data =['id'=>base64_decode($_POST['token'])];
    $response =$api->GetResponse();
    $response =json_decode($response,true);
    $alert =($response['status'])?'alert-success':'alert-danger';
    echo"<div class='alert $alert'>".$response['message']."</div>";
  }
  $api->api = "flight-list.php";
  $api->method = "GET";
  $api->data =['page'=>(isset($_GET['page']))?$_GET['page']:1];
  $response =$api->GetResponse();
  $response =json_decode($response,true);
?>
    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col">SR</th>
                <th scope="col">Flight No</th>
                <th scope="col">Flight Name</th>
                <th scope="col">Type</th>
                <th scope="col">Seat</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
        <?php
                if($response['status']==false)
                {
                    echo "<tr><td colspan='5'>".$response['message']."</td></tr>";
                }
                else{
                    $i=(isset($_GET['page']))?(($_GET['page']-1)*$response['per_page'])+1:1;
                    foreach($response['data'] as $data)
                    {
                        echo"
                        <tr>
                            <td>".$i++.". </td>
                            <td>".$data['flight_no']."</td>
                            <td>".$data['flight_name']."</td>
                            <td>".$data['type']."</td>
                            <td>".$data['seat']."</td>";
                            $id =base64_encode($data['id']);
                            ?>
                            <td>
                                <div class="btn-group">
                                <a href="view-schedule.php?flight_id=<?php echo $id; ?>"  class="btn btn-info">View Details</a>
                                <a href="edit-flight.php?flight_id=<?php echo $id; ?>"  class="btn btn-primary">Update</a>
                                <button data-mdb-toggle="modal" data-mdb-target="#deleteModal" onclick="deleteFlight('<?php echo $id; ?>')" class="btn btn-danger">Delete</button>
                            </div></td>
                        </tr>
                        <?php
                    }
                }
            ?>    
        </tbody>
    </table>
    <nav aria-label="...">
      <ul class="pagination pagination-circle">
        <?php
        if($response['status'])
        for($i=0;$i<$response['count'];$i++)
        {
          $active =(isset($_GET['page'])) ? $_GET['page'] : 0+1;
          $active =($active==$i+1)?'active':'';
          echo'<li class="page-item '.$active.'"><a class="page-link" href="flight-list.php?page='.($i+1).'">'.($i+1).'</a></li>';
        }
        ?>
      </ul>
    </nav>
  </div>
  <!-- Jumbotron -->
</header>
<!-- Button trigger modal -->

<!-- Modal -->
<div
  class="modal fade"
  id="deleteModal"
  tabindex="-1"
  aria-labelledby="exampleModalLabel"
  aria-hidden="true"
>
<form method="post">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Are you sure to delete this flight ?</h5>
        <input type="hidden" name="token" class="token">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-mdb-dismiss="modal">
          No
        </button>
        <button name="delete" class="btn btn-primary">Yes Delete</button>
      </div>
    </div>
  </div>
  </form>
</div>
</div>
<script  type="text/javascript"  src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.2.0/mdb.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    function deleteFlight(token)
    {
        $('.token').val(token);
    }
</script>
</body>
</html>